/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   libDrive.cpp
 * Author: heisenberg
 * 
 * Created on 6 November 2019, 12:58
 */
#include <cstdlib>
#include <iostream>
#include "wiringPi.h"
#include "softPwm.h"
#include "pthread.h"
#include "libDrive.h"

libDrive::libDrive() {
}

libDrive::libDrive(const libDrive& orig) {
}

libDrive::~libDrive() {
}

void libDrive::init(int pin1For, int pin1Rev, int pin2For, int pin2Rev, int pinEnable)
{
    //right wheel
    if(((pin1For != 1)&&(pin1For != 26)&&(pin1For != 23))||((pin2For != 1)&&(pin1For != 23)&&(pin1For != 26)))
    {
        std::cout<<"Error: pin1For and pin2For must be a hardware pwm pins"<<std::endl; 
        return; 
    }
    this->pin1For = pin1For; 
    this->pin1Rev = pin1Rev; 
    //left wheel
    this->pin2For = pin2For; 
    this->pin2Rev = pin2Rev; 
    this->pinEnable = pinEnable; 
    this->speed = 0; 
    //right wheel
    this->speed1 = 0; 
    //left wheel
    this->speed2 = 0; 
    //right wheel
    this->head1 = 0; 
    //left wheel
    this->head2 = 0; 

    this->forward = 1; 
    
    // initialise all as pwm outputs with initial duty cycle of zero 
    pinMode(this->pin1For, PWM_OUTPUT); 
    pinMode(this->pin2For, PWM_OUTPUT); 
    pwmSetMode(PWM_MODE_MS); 
    pwmSetClock(this->clock);
    pwmSetRange(this->rangeFor);   
    pwmWrite(this->pin1For, 0);     
    pwmWrite(this->pin2For, 0); 
    softPwmCreate(this->pin1Rev,0,this->rangeRev); 
    softPwmCreate(this->pin2Rev,0,this->rangeRev); 
    
    pinMode(this->pinEnable, OUTPUT); 
    digitalWrite(this->pinEnable, 0); 


}

 void libDrive::vel(int speed)
 {
     
     
     if(speed <= this->range && speed >= 0)
     {
         speed1 = speed; 
         speed2 = speed; 
         forward = 1; 
         
         this->vel1(); 
         this->vel2(); 
     }else if(speed >= -range && speed <0){
         
         speed1 = speed; 
         speed2 = speed; 
         forward = 0; 
         this->vel1(); 
         this->vel2(); 
         
     }else{
         //stop
         speed1 = 0; 
         speed2 = 0; 
         forward = 1; 
         this->vel1(); 
         this->vel2(); 
         std::cout<<"Invalid speed entered! Stopping motors. "<<std::endl; 
         
     }
 }
 
 void libDrive::turn(int heading)
 {
     if(heading > range || heading < -range)
     {
         std::cout<<"range must be a value between "<< heading << " and " << -heading << std::endl; 
         return; 
     }

     if(heading > 0 && forward)
     {
         head1 = -heading; 
         head2 = 0; 
     }else if(heading < 0 && forward)
     {
         head1 = 0; 
         head2 = heading; 
     }else if(heading > 0 && !forward){
         head1 = 0; 
         head2 = heading; 
     }else if(heading < 0 && !forward){
         head1 = -heading; 
         head2 = 0; 
     }else{
         head1 = 0; 
         head2 = 0; 
     }
         
     this->vel1(); 
     this->vel2(); 
     
     
 }
 
 void libDrive::enable(bool enableDrive)
 {
     if(enableDrive)
         digitalWrite(this->pinEnable, 1); 
     else
         digitalWrite(this->pinEnable, 0); 
 }


 void libDrive::vel1()
 {
     if((speed1+head1) > 0)
     {
         pwmWrite(pin1For, ((speed1 + head1)*0.6 + 4)*(rangeFor/range) + correctFor); 
         softPwmWrite(pin1Rev, 0); 
 //         std::cout<<"forwards duty1 "<<((speed1 + head1)*0.6 + 4)*(rangeFor/range) + correctFor<<std::endl; 
     }else if((speed1 + head1) < 0){
         pwmWrite(pin1For, 0); 
         softPwmWrite(pin1Rev, -((speed1 + head1)*0.8 - 4 - correctRev)); 
  //       std::cout<<"backwards duty1"<<(-((speed1 + head1)*0.8 - 4 - correctRev))<<std::endl; 
     }else{
         pwmWrite(pin1For, 0); 
         softPwmWrite(pin1Rev, 0); 
         //~ std::cout<<"stopped 1"<<std::endl;        
     }
 }
 
 
 void libDrive::vel2()
 {
     if((speed2+head2) > 0)
     {
         pwmWrite(pin2For, ((speed2 + head2)*0.6 + 4)*(rangeFor/range)); 
         softPwmWrite(pin2Rev, 0); 
          //~ std::cout<<"forwards duty2 "<<((speed2 + head2)*0.6 + 4)*(rangeFor/range)<<std::endl; 
          //~ std::cout<<"speed2 "<<speed2<<std::endl; 
          //~ std::cout<<"head2 "<<head2<<std::endl; 

     }else if((speed2 + head2) < 0){
         pwmWrite(pin2For, 0); 
         softPwmWrite(pin2Rev, -((speed2 + head2)*0.8 - 4)); 
          //~ std::cout<<"backwards duty2 "<<-((speed2 + head2)*0.8 - 4)<<std::endl; 

     }else{
         pwmWrite(pin2For, 0); 
         softPwmWrite(pin2Rev, 0); 
         //~ std::cout<<"stopped 2"<<std::endl;        
     }
 }
 
 int libDrive::getRange()
 {
      return this->range; 
 }
