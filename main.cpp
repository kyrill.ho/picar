/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: heisenberg
 *
 * Created on 31 October 2019, 07:56
 */

#include <cstdlib>
#include <iostream>
#include <signal.h>
#include <math.h>
#include <wiringPi.h>
#include "libSonar.h"
#include "libServo.h"
#include "libDrive.h"

using namespace std;

// ultrasound variables 
int trigger1 = 25;
int echo1 = 24;
int trigger2 = 29; 
int echo2 = 28; 

double dist1 = 0; 
double dist2 = 0; 
double distDifAv = 0; 

// servo variables 
int servoPin = 5; 
int dutyCycle = 13; 

// DC control variables 
int pin1For = 23; 
int pin1Rev = 21; 
int pin2For = 26; 
int pin2Rev = 22; 

// input variables 
bool inputReceived = false; 
int input = 4; 

// End program variables
int pinEnable = 4; 
bool interruptReceived = 0; 

// Control threshold variables 
int maxDist = 35; 
int minDist = 10; 

//**** PID control variables ******
double Kp = 5; // proportional gain
double Kd = 2;   // derivative gain
double N = 20;            // filter coefficients
double Ts = 0.1;       //sampling time PID
double a0 = (1+N*Ts);
double a1 = -(2 + N*Ts);
double a2 = 1;
double b0 = Kp*(1+N*Ts) + Kd*N;
double b1 = -(Kp*(2+N*Ts) + 2*Kd*N);
double b2 = Kp + Kd*N;
double ku1 = a1/a0; 
double ku2 = a2/a0; 
double ke0 = b0/a0; 
double ke1 = b1/a0; 
double ke2 = b2/a0;
double maxPIDRange = 800; 

void mainControlPID(Servo *servo, libDrive *power); 
void pidControl(Servo *servo, libDrive *power); 
void mainControlRough(Servo *servo, libDrive *power); 
PI_THREAD (updateDist); 
PI_THREAD (getInput); 
void signalHandler(int sigNum); 
void testUltrasonic(); 
void testServo(Servo *servo); 
void testDC(libDrive *power); 

int main()
{
    // register signal handling function 
    signal(SIGINT, signalHandler); 
    
    // initialise wiringpi
    if (wiringPiSetup() == -1)
        return -1;
   
   // start ultrasonic measurment thread
    int x = piThreadCreate (updateDist) ;
    if (x != 0)
        cout<<"Ultrasonic modules did not start."<<endl;
        
    // initialise servo unit 
    Servo servo; 
    servo.init(servoPin);
    
    
    // start input thread
    x = piThreadCreate (getInput) ;
    if (x != 0)
        cout<<"Input thread did not start"<<endl; 
    
    // initialise driving unit and enable    
    libDrive power; 
    power.init(pin1For, pin1Rev, pin2For, pin2Rev, pinEnable); 
    power.enable(1);
       
    // begin functionality
    mainControlRough(&servo, &power); 
    return 0; 
 
}

// main control loops 
void mainControlPID(Servo *servo, libDrive *power)
{
    delay(2000); 
    while(1)
    {

        if(dist1 > maxDist and dist2 > maxDist) 
        {
            servo->writeAngle(servoPin, 13); 
            power->turn(0); 
            power->vel(input); 
            cout<<"Far away, driving straight"<<endl; 
        }else if(dist1 < minDist or dist2 < minDist){
            servo->writeAngle(servoPin, 13); 
            power->turn(0); 
            power->vel(-3); 
            cout<<"Too close, backing up"<<endl; 
            cout<<"dist1 "<<dist1<<endl; 
            cout<<"dist2 "<<dist2<<endl;
        }else{
            pidControl(servo, power); 
        }
       
        
        if(interruptReceived)
        {
            cout<<"Interrupting program."<<endl;
            servo->writeAngle(servoPin, 13); 
            power->vel(0);
            power->turn(0); 
            power->enable(0);  
            return; 
        }
        delay(50); 
    }
}

// pid control function 
void pidControl(Servo *servo, libDrive *power)
{
    
    // global variables
    double e2 = 0; 
    double e1 = 0; 
    double u2 = 0; 
    double u1 = 0; 
    double u0 = 0;    
    
    double steerTo = 0;                 



    while(1){

        e2=e1; 
        e1=distDifAv; 
        u2=u1;       
        u1=u0;
        u0 = (-ku1*u1 - ku2*u2 + ke0*distDifAv + ke1*e1 + ke2*e2); 
        steerTo = (u0/maxPIDRange)*7; 

        if (steerTo > power->getRange() - 4)
        { 
            steerTo = power->getRange() - 4;  // limit steering range
        }else if (steerTo < -(power->getRange() - 4)){
            steerTo = -(power->getRange() - 4); 
        }
        
        power->turn(steerTo); 
        power->vel(input); 
        servo->writeAngle(servoPin, (13 + steerTo*0.4)); 
        
        if(interruptReceived)
            return; 
            
        if(dist1 > maxDist and dist2 > maxDist)
            return; 
            
        else if(dist1 < minDist or dist2 < minDist)
            return; 
            
        cout<<"PID output is "<<((u0/maxPIDRange)*7)<<endl; 
        delay(80); 


    }
}

void mainControlRough(Servo *servo, libDrive *power)
{
    delay(2000); 
    while(1)
    {

        if(dist1 > maxDist and dist2 > maxDist) 
        {
            servo->writeAngle(servoPin, 13); 
            power->turn(0); 
            power->vel(input); 
            cout<<"Far away, driving straight"<<endl; 
        }else if(dist1 > maxDist and dist2 < maxDist)
        {
            if(dist2 < minDist) 
            {
                power->turn(-7); 
                servo->writeAngle(servoPin, 13 - 4); 
            }else{
                power->turn(-3); 
                servo->writeAngle(servoPin, 13 - 2); 
            }
            power->vel(input); 
            cout<<"Close to something on right side"<<endl; 
        }else if(dist1 < maxDist and dist2 > maxDist)
        {
            if(dist1 < minDist) 
            {
                power->turn(7); 
                servo->writeAngle(servoPin, 13 + 4); 
            }else{
                power->turn(3); 
                servo->writeAngle(servoPin, 13 + 2); 
            }
            power->vel(input); 
            cout<<"close to somethings on left side"<<endl; 
        }else{
            power->vel(-3); 
            servo->writeAngle(servoPin, 13); 
            cout<<"Too close, backing up"<<endl; 
            while(!(dist1 > maxDist and dist2 > maxDist))
            {
                if(interruptReceived)
                {
                    break; 
                }
            } 
        }
        
        if(interruptReceived)
        {
            servo->writeAngle(servoPin, 13); 
            power->vel(0);
            power->turn(0); 
            power->enable(0);  
            return; 
        }
        delay(50); 
    }
}
// multithreading for distance measurement 
PI_THREAD (updateDist)
{
        Sonar sonar1;
        sonar1.init(trigger1, echo1);
        Sonar sonar2; 
        sonar2.init(trigger2, echo2);
        piHiPri(99); 
        
        double distDifFilter [5] = {0,0,0,0,0};
        double distDif = 0; 
        double temp = 0; 

        
        while(1)
        {
            distDif = 0; 
            delay(100); 
            dist1 = sonar1.distance(3000); 
            delay(100);
            dist2 = sonar2.distance(3000); 
            
            //push filter buffer from back to front 
            for(int i = 0; i < 4; i++)
            {
                distDifFilter[i] = distDifFilter[i+1]; 
            }
            
            distDifFilter[4] = dist1-dist2; 
            
            for(int i = 0; i < 5; i++)
            {
                distDif += distDifFilter[i]; 
            }
            
            distDifAv = distDif/5;
            //cout<<distDifAv<<endl; 
        }
}

PI_THREAD(getInput)
{
    piHiPri(99); 
    while(1)
    {
        cout<<"Enter value: "<<endl; 
        cin>>input; 
        inputReceived = true; 
    }
}


void signalHandler(int sigNum)
{
    cout<<" Signal "<<sigNum<<" received."<<endl;
    interruptReceived = 1; 
    
}



/******************* Testing functions go here ************************/


void testUltrasonic()
{
    
 


    while(1){

       
        //~ delay(2000);
        
        
        //~ cout << "Distance 1 is " << dist1 << " cm." << endl;
        //~ cout << "Distance 2 is " << dist2 << " cm." << endl;
        //~ cout << "Average difference is " << distDifAv << " cm." << endl;

        
        if(interruptReceived)
        {
            break; 
        }
    }
}

void testServo(Servo *servo)
{
    
    
    
    // 6 - 23 seems to work, not giving 180 degrees though
    // with ultrasound mounted. 13 is the middle. 9 is right, 18 is left. 
    while(1)
    {
        if(inputReceived)
        {
            servo->writeAngle(servoPin, input); 
            inputReceived = false; 
        }
        
        if(interruptReceived)
        {
            servo->writeAngle(servoPin, 13); 
            break; 
        }
    }
}

void testDC(libDrive *power)
{
    
    int vel = 0; 
    int dir = 0; 
    power->vel(vel); 

    int x = piThreadCreate (getInput) ;
    if (x != 0)
        printf ("it didn't start");
    
    while(1)
    {
        
        if(inputReceived)
        {
            power->vel(input); 
            inputReceived = false; 
        }
        
        
        if(interruptReceived)
        {
            power->turn(0); 
            power->vel(0); 
            power->enable(0); 
            break; 
        }
        delay(50); 
    }
    
}
