/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   libSonar.h
 * Author: heisenberg
 *
 * Created on 2 November 2019, 18:30
 */

#ifndef LIBSONAR_H
#define LIBSONAR_H

class Sonar
{
  public:
    Sonar();
    void init(int trigger, int echo);
    double distance(int timeout);

  private:
    void recordPulseLength();
    int trigger;
    int echo;
    volatile long startTimeUsec;
    volatile long endTimeUsec;
    double distanceMeters;
    long travelTimeUsec;
    long now;
    static const long maxDist = 50; 
};


#endif /* LIBSONAR_H */

