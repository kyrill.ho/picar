/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   libDrive.h
 * Author: heisenberg
 *
 * Created on 6 November 2019, 12:58
 */

#ifndef LIBDRIVE_H
#define LIBDRIVE_H

class libDrive {
public:
    libDrive();
    libDrive(const libDrive& orig);
    virtual ~libDrive();
    
    // controls both motors. Range of 10 gives 1kHz
    void init(int pin1For, int pin1Rev, int pin2For, int pin2Rev, int pinEnable); 
    
    // speed can be negative for reverse
    void vel(int speed); 
    
    // turn changes relative speed of the motors but not their actual speed
    void turn(int heading); 
    void enable(bool enableDrive); 
    // getters and setters
    int getRange(); 
    
private:
    int pin1For; 
    int pin2For; 
    int pin1Rev; 
    int pin2Rev; 
    int pinEnable; 
    static const int rangeFor = 320; 
    static const int clock = 6; 
    static const int range = 10; 
    static const int rangeRev = 12; 
    double speed; 
    double speed1; 
    double speed2; 
    static const int correctFor = 0; 
    static const int correctRev = 0; 
    double head1; 
    double head2; 
    
    
    
    // true for stopped or driving forward. 
    bool forward; 
    
    // functions for controlling speed separately 
    void vel1(); 
    void vel2(); 
    


};

#endif /* LIBDRIVE_H */

