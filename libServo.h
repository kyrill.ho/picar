/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   libServo.h
 * Author: heisenberg
 *
 * Created on 3 November 2019, 13:12
 */

#ifndef LIBSERVO_H
#define LIBSERVO_H

class Servo{
    public:
        Servo(); 
        void init(int pinOut, int pwmSig = 13); 
        void writeAngle(int pinOut, int pwmSig); 
        
    private: 
        int pin;
        int pwmSig; 
        static const int range = 200; //static -> stored in static memory so available to all instances
    
};

#endif /* LIBSERVO_H */

