/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   libServo.cpp
 * Author: heisenberg
 *
 * Created on 3 November 2019, 14:44
 */
#include <cstdlib>
#include <iostream>
#include "wiringPi.h"
#include "softPwm.h"
#include "pthread.h"
#include "libServo.h"

Servo::Servo(){}

void Servo::init(int pinOut, int pwmSig)
{
    
    this->pin = pinOut;

    softPwmCreate(pin,pwmSig,range); 

}

void Servo::writeAngle(int pinOut, int pwmSig)
{
    this->pin = pinOut;
    this->pwmSig = pwmSig;

 //   std::cout<<"duty is "<<pwmSig<<std::endl; 
    softPwmWrite(pin, pwmSig);
}
